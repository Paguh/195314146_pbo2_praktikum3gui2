package prak3b;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author asus
 */
public class ch14JListSample extends JFrame {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 250;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private JList list;

    public static void main(String[] args) {
        ch14JListSample frame = new ch14JListSample();
        frame.setVisible(true);
    }

    public ch14JListSample() {
        Container contentPane;
        JPanel listPanel, okPanel;
        JButton okButton;

        String[] names = {"APE", "BAT", "BEE", "CAT", "DOG", "EEL", "FOX", "GNU", "HEN", "MAN", "SOW", "YAK"};
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setTitle("Program ch14JlistSample");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);
        contentPane = getContentPane();
        contentPane.setBackground(Color.WHITE);
        contentPane.setLayout(new BorderLayout(2, 3));

        listPanel = new JPanel(new GridLayout(0, 1));
        listPanel.setBorder(BorderFactory.createTitledBorder("Three Little animal name"));

        list = new JList(names);
        listPanel.add(new JScrollPane(list));

        okPanel = new JPanel(new FlowLayout());
        okButton = new JButton("ok");
        okPanel.add(okButton);

        contentPane.add(listPanel, BorderLayout.CENTER);
        contentPane.add(okPanel, BorderLayout.SOUTH);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}
